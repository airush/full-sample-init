<html>
<head>
    <title>Spring MVC Java Based Config Hello World</title>
</head>

<body>
<h1>All users</h1>
<ul>
    <#list users as user>
        <li>
            <h3>${user.username}</h3>
        </li>
    </#list>
</ul>
</body>
</html>