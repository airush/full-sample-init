package com.gmail.aliveairush.dao;

import com.gmail.aliveairush.model.User;

import java.util.List;

public interface UserDao {
    List<User> getAllUsers();
}
