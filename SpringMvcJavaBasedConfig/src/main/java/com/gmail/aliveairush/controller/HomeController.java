package com.gmail.aliveairush.controller;


import com.gmail.aliveairush.dao.UserDao;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HomeController {

    private final UserDao userDao;

    public HomeController(UserDao userDao) {
        this.userDao = userDao;
    }

    @RequestMapping("/")
    public String welcomePage(){
        return "index";
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public String getAllUsersPage(Model model){

        model.addAttribute("users", userDao.getAllUsers());
        return "user-list";
    }
}
