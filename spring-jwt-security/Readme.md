### Spring simple jwt authentication
 ___
 
 Skipping part about how client get jwt.
 
#### Jwt workflow

 1. Client set `Authorization` Header with `Bearer <our-jwt>`
 2. "JwtAuthenticationFilter" delegate to `JwtAuthenticationProvider` resolve jwt and transform it to to `UserDetailsImpl`
 3. Then authFilter creates `UsernamePasswordAuthenticationToken` from `UserDetailImpl`
 4. Passing `UsernamePasswordAuthenticationToken` to SpringContextHolder. Done.
 

 **`UserDetailImpl`** - required information to identify and authorize User in this example. Its more like Principal and 
 Credentials the same time. Actually it should be separated if I would use custom Authorization, but as Spring Docs
says UserDetails often is used as Authentication's Principal.

 **`UsernamePasswordAuthenticationToken`** - default Spring's Authentication implementation. Represent Username, password, and List of GrantedAuthorities.
 
 **`JwtAuthenticationProvider`** -  class that has to *authenticate* user, creates Jwt and decodes it.