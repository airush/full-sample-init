package gmail.aliveairush.springjwtsecurity.security;

import gmail.aliveairush.springjwtsecurity.model.Role;
import gmail.aliveairush.springjwtsecurity.model.User;
import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

// Even though it is not implementing AuthManager it does his job, it authenticate out token
@Component
public class JwtAuthenticationProvider {

    private static final Logger logger = LoggerFactory.getLogger(JwtAuthenticationProvider.class);

    // Hardcoded! should be here.
    private Integer jwtLifeTime = 300000;
    private String secretKey = "neverDoLikeThat";

    public String generateJwtToken(User user) {
        String rolesSeparatedByComma = user.getRoles().stream()
                .map(Role::getAuthority)
                .collect(Collectors.joining(","));

        Claims claims = Jwts.claims().setSubject(user.getUsername());
        claims.put("rol", rolesSeparatedByComma);
        claims.put("uid", user.getId());

        Date now = new Date();
        Date expiryTime = new Date(now.getTime() + jwtLifeTime);

        return Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(now)
                .setExpiration(expiryTime)
                .signWith(SignatureAlgorithm.HS256, secretKey)
                .compact();
    }

    public String resolveToken(HttpServletRequest request) {
        String authHeader = request.getHeader("Authorization");
        if (StringUtils.hasText(authHeader) && authHeader.startsWith("Bearer "))
            return authHeader.substring(7);
        return null;
    }

    public boolean validateToken(String jwt) {
        try {
            Jwts.parser().setSigningKey(secretKey).parseClaimsJws(jwt);
            return true;
        } catch (SignatureException ex) {
            logger.error("Invalid JWT signature");
        } catch (MalformedJwtException ex) {
            logger.error("Invalid JWT token");
        } catch (ExpiredJwtException ex) {
            logger.error("Expired JWT token");
        } catch (UnsupportedJwtException ex) {
            logger.error("Unsupported JWT token");
        } catch (IllegalArgumentException ex) {
            logger.error("JWT claims string is empty.");
        }
        return false;
    }

    public UserDetails decodeToUserDetails(String jwt) {

        Claims claims = Jwts.parser()
                .setSigningKey(secretKey)
                .parseClaimsJws(jwt)
                .getBody();
        String username = claims.getSubject();
        Long userId = Long.valueOf((Integer) claims.get("uid"));
        List<? extends GrantedAuthority> authorities = AuthorityUtils.commaSeparatedStringToAuthorityList((String) claims.get("rol"));
        return new UserDetailsImpl(userId, username, authorities);
    }
}
