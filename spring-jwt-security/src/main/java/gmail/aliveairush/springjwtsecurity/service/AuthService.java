package gmail.aliveairush.springjwtsecurity.service;

import gmail.aliveairush.springjwtsecurity.model.User;
import gmail.aliveairush.springjwtsecurity.repository.UserRepository;
import gmail.aliveairush.springjwtsecurity.security.JwtAuthenticationProvider;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class AuthService {

    private final JwtAuthenticationProvider jwtAuthenticationProvider;

    private final PasswordEncoder passwordEncoder;

    private final UserRepository userRepository;

    public AuthService(JwtAuthenticationProvider jwtAuthenticationProvider, PasswordEncoder passwordEncoder, UserRepository userRepository) {
        this.jwtAuthenticationProvider = jwtAuthenticationProvider;
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
    }

    public String generateJwtToken(String username, String password) {

        User user = userRepository.findByUsername(username);

        if (passwordEncoder.matches(password, user.getPassword()))
            return jwtAuthenticationProvider.generateJwtToken(user);
        else throw new IllegalArgumentException("Incorrect login or password");
    }
}
