package gmail.aliveairush.springjwtsecurity.repository;

import gmail.aliveairush.springjwtsecurity.model.Role;
import gmail.aliveairush.springjwtsecurity.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

// Hardcoded because don't want to add Database for this example
@Repository
public class UserRepository {

    private List<User> users = new ArrayList<>();

    public UserRepository(PasswordEncoder pswdEncoder){
        User alice = new User(1L , "alice", "alice@gmail.com", pswdEncoder.encode("alice"), Collections.singletonList(Role.ROLE_ADMIN));
        User bob = new User(2L , "bob", "bob@gmail.com", pswdEncoder.encode("bob"), Collections.singletonList(Role.ROLE_CLIENT));
        users.add(alice);
        users.add(bob);
    }


    public User findByUsername(String username) {

        return users.stream().filter(o-> o.getUsername().equals(username)).findFirst().get();
    }
}
