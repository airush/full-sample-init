package gmail.aliveairush.springjwtsecurity.controller;

import gmail.aliveairush.springjwtsecurity.security.UserDetailsImpl;
import gmail.aliveairush.springjwtsecurity.service.AuthService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.Map;

@RestController
public class GodController {

    private static final Logger logger = LoggerFactory.getLogger(GodController.class);

    @Autowired
    private AuthService authService;

    @GetMapping("/hello")
    public String httpGetHello() {
        logger.debug("Hello request");
        return "Hello authenticated user";
    }

    @GetMapping("/user")
    public String httpGetUserPage() {

        logger.debug("User request");
        return "Users";
    }

    @GetMapping("/admin")
    public String httpGetAdminPage() {
        UserDetailsImpl userDetails = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        logger.debug("Request by uid={}, username={}, role={}", userDetails.getUserId(), userDetails.getUsername(), userDetails.getAuthorities());
        return "Admin";
    }

    @PostMapping("/login")
    public String httpPostSignIn(@RequestBody Map<String, String> requestBodyMap) {
        try {
            logger.debug("Login request user= {} , password= {}", requestBodyMap.get("username"), requestBodyMap.get("password"));
            return authService.generateJwtToken(requestBodyMap.get("username"), requestBodyMap.get("password"));
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

}
