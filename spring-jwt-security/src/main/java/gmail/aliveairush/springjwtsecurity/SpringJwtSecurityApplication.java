package gmail.aliveairush.springjwtsecurity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.UserDetailsServiceAutoConfiguration;

// Disable "Using default security password"
@SpringBootApplication(exclude = {UserDetailsServiceAutoConfiguration.class})
public class SpringJwtSecurityApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringJwtSecurityApplication.class, args);
    }

}
