package com.gmail.aliveairush.fullsample.controllers;

import com.gmail.aliveairush.fullsample.services.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = "/users/{user-id}")
    public ResponseEntity<String> getUsernameByUserId(@PathVariable(value = "user-id") Long id) {
        return new ResponseEntity<>(userService.getUsernameById(id), HttpStatus.OK);
    }
}
