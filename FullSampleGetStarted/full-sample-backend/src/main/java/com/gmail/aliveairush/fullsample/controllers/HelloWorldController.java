package com.gmail.aliveairush.fullsample.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class HelloWorldController {

    @GetMapping(value = "/hello")
    public ResponseEntity<String> getHelloMessage(){
        return ResponseEntity.ok().body("Hello");
    }
}
