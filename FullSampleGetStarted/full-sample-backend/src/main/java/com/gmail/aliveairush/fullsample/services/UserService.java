package com.gmail.aliveairush.fullsample.services;

import com.gmail.aliveairush.fullsample.models.User;
import com.gmail.aliveairush.fullsample.repositories.UserRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public String getUsernameById(Long id){
        Optional<User> oUser = userRepository.findById(id);
        return oUser.isPresent()? oUser.get().getUsername() : null;
    }
}
