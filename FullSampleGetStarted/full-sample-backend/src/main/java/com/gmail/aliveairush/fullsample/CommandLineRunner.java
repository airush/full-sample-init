package com.gmail.aliveairush.fullsample;

import com.gmail.aliveairush.fullsample.models.User;
import com.gmail.aliveairush.fullsample.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CommandLineRunner implements org.springframework.boot.CommandLineRunner {

    @Autowired
    private UserRepository userRepository;

    @Override
    public void run(String... args) throws Exception {
        User user = new User();
        user.setName("Sam");
        user.setUsername("qwerty");
        userRepository.save(user);
    }
}
