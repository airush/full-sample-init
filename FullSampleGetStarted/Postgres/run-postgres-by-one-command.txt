### Run postgres container from command line

##Create volume with name PostgresData. To use in docker run add: --volumes-from PostgresData
#docker create -v /var/lib/postgresql/data --name PostgresData alpine

## Create folder to save volumes
#mkdir -p $HOME/Main/docker/volumes/postgres

##Run postgres
docker run -p 5432:5432 --name postgres -e POSTGRES_USER=airush -e POSTGRES_PASSWORD=admin -e POSTGRES_DB=fullsampledb -d -v $HOME/Main/docker/volumes/postgres:/var/lib/postgresql/data postgres

##Connect to postgres console 
docker exec -it <container_id> /bin/sh
