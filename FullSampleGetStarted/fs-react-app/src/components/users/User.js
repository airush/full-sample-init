import React, { Component } from "react";
import axios from "axios";

class User extends Component {
  state = {};

  componentDidMount() {
    axios
      .get("http://localhost:8080/hello")
      .then(resp => this.setState({ message: resp.data }));
      axios
      .get("http://localhost:8080/users/1")
      .then(resp => this.setState({ name: resp.data }));
  }

  render() {
    return (
      <div>
        <div>Test http request from server</div>
        <h1>{this.state.message}</h1>
        <h2>{this.state.name}</h2>
      </div>
    );
  }
}

export default User;
