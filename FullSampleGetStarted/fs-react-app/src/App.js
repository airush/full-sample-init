import React, { Component } from "react";
import "./App.css";
import "./components/users/User";
import User from "./components/users/User";

class App extends Component {
  render() {
    return (
      <div className="App">
        <User></User>
      </div>
    );
  }
}

export default App;
