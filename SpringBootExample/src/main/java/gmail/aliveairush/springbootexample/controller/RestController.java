package gmail.aliveairush.springbootexample.controller;

import gmail.aliveairush.springbootexample.converter.UserConverter;
import gmail.aliveairush.springbootexample.dao.UserDao;
import gmail.aliveairush.springbootexample.dto.UserDto;
import gmail.aliveairush.springbootexample.model.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;
import java.util.stream.Collectors;

@org.springframework.web.bind.annotation.RestController
@Api(description="Testing Rest controller to say hello and return users")
public class RestController {

    private final UserDao userDao;

    private final UserConverter userConverter;

    @Autowired
    public RestController(UserDao userDao, UserConverter userConverter) {
        this.userDao = userDao;
        this.userConverter = userConverter;
    }

    @GetMapping("/api/hello")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful request"),
            @ApiResponse(code = 401, message = "You are not authorized"),
            @ApiResponse(code = 403, message = "Accessing the resource is forbidden"),
            @ApiResponse(code = 404, message = "Resource is not found")
    })
    @ApiOperation(value = "Just says 'hello'")
    public String helloRestMapping(){
        return "Hello";
    }

    @GetMapping("/api/users")
    @ApiOperation(value = "Returns a list of all available users", response = List.class)
    public ResponseEntity<?> getAllUsers(){
        List<User> users = userDao.getAllUsers();
        List<UserDto> userDtos = users.stream()
                .map(userConverter::convertToDto)
                .collect(Collectors.toList());

        return ResponseEntity.ok(userDtos);
    }
}
