package gmail.aliveairush.springbootexample.controller;

import gmail.aliveairush.springbootexample.dao.UserDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeController {

    private Logger logger = LoggerFactory.getLogger(HomeController.class);

    private final UserDao userDao;

    public HomeController(UserDao userDao) {
        this.userDao = userDao;
    }

    @GetMapping("/")
    public ModelAndView welcomePage(){
        return new ModelAndView("index");
    }

    @GetMapping("/users")
    public String getAllUsersPage(Model model){
        model.addAttribute("users", userDao.getAllUsers());
        logger.info("GET all users request!");
        logger.debug("Got {} users", userDao.getAllUsers().size());
        return "user-list";
    }
}
