package gmail.aliveairush.springbootexample.converter;

import gmail.aliveairush.springbootexample.dto.UserDto;
import gmail.aliveairush.springbootexample.model.User;
import org.springframework.stereotype.Component;

@Component
public class UserConverter extends EntityToDtoConverter<User, UserDto> {

    @Override
    public UserDto convertToDto(User entity) {
        UserDto userDto = modelMapper.map(entity, UserDto.class );
        return userDto;
    }

    @Override
    public User convertToEntity(UserDto dto) {
        return null;
    }
}
