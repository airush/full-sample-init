package gmail.aliveairush.springbootexample.converter;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public abstract class EntityToDtoConverter<S, T> {
    protected final ModelMapper modelMapper = new ModelMapper();

    public abstract T convertToDto(S entity);
    public abstract S convertToEntity(T dto);
}
