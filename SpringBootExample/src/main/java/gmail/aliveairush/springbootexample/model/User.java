package gmail.aliveairush.springbootexample.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "User details")
public class User {
    @ApiModelProperty(notes = "The user username/login")
    private String username;

    private Long id;

    private String password;

    public User(Long id, String username,  String password) {
        this.username = username;
        this.id = id;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public Long getId() {
        return id;
    }

    public String getPassword() {
        return password;
    }
}
