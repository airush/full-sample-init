package gmail.aliveairush.springbootexample.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class Swagger2Config {

    @Bean
    public Docket apiDocket(){
        return new Docket(DocumentationType.SWAGGER_2)
                .select() // returns ApiSelectorBuilder
                .paths(PathSelectors.any()) // may change to .ant("/api/")
                .apis(RequestHandlerSelectors.basePackage("gmail.aliveairush"))
                .build()
                .apiInfo(apiEndPointsInfo()); // customization
    }

    private ApiInfo apiEndPointsInfo() {
        return new ApiInfoBuilder().title("Spring Boot Example")
                .description("Spring Boot example REST API")
                .contact(new Contact("Ainur Aimurzin", "", "aliveairush@gmail.com"))
                .version("1.0.0")
                .build();
    }

}
