package gmail.aliveairush.springbootexample.dao;

import gmail.aliveairush.springbootexample.model.User;
import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.List;

@Repository
public class UserDaoImpl implements UserDao {

    private static List<User> users;

    static {
        users = Arrays.asList(
                new User(1L, "Alice", "qwerty1"),
                new User(2L,"Bob","qwerty2"),
                new User(3L,"Clay", "qwerty3"));
    }

    public List<User> getAllUsers() {
        return users;
    }
}
