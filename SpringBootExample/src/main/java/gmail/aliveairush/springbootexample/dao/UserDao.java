package gmail.aliveairush.springbootexample.dao;

import gmail.aliveairush.springbootexample.model.User;

import java.util.List;

public interface UserDao {
    List<User> getAllUsers();
}
