## Spring MVC XML config HelloWorld example

1. 1) Download tomcat: https://tomcat.apache.org/
1. 2) Grand permission to catalina:  chmod a+x /path/to/tomcat/bin/catalina.sh 


2) Create artifact: File -> Project Structure -> Artifacts ->  +  ->  WebApplication war Exploded
3) Define run configuration: Choice Tomcat -> Set path to tomcat folder  and war

*Notice!) File -> Project Structure -> Web Resource Directories -> Path Relative to Deployment Root shout be `/`, not `/WEB-INF`.  Once I lost a few hours on that.   :|*
