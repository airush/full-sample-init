package com.gmail.aliveairish.dao;

import com.gmail.aliveairish.model.User;

import java.util.List;

public interface UserDao {
    List<User> getAllUsers();
}
