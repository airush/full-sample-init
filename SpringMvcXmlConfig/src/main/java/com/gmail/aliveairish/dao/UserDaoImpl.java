package com.gmail.aliveairish.dao;

import com.gmail.aliveairish.model.User;
import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.List;

@Repository
public class UserDaoImpl implements UserDao {
    public List<User> getAllUsers() {
        return Arrays.asList(new User("Alice"), new User("Bob"), new User("Clay"));
    }
}
