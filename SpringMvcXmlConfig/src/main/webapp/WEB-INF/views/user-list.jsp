<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<html>
<head>
	<title>Spring MVC Hello World</title>
</head>

<body>
	<h1>All users</h1>
	<ul>
		<c:forEach items="${users}" var="user">
			<li>
				<td><h3>${user.username}</h3></td>
			</li>
		</c:forEach>
	</ul>
</body>
</html>