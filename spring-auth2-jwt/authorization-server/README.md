### Authorization Server - Spring & OAuth2 & JWT 
___

#### How to get jwt token
To get JWT token make POST request in postman to AuthorizationServer:
1) Choose Authorization -> Choose `Basic auth`: there set client-id name and client-secret. And then on url `http://@localhost:8080/oauth/token` put user data to the body: `grant_type: password`, `user: username`, `password: password`
2) Or do the same, but send request to `http://`**`client-id:client-secret`**`@localhost:8080/oauth/token` instead of declaring Basic auth. 

**Example** POST request to `http://aliveairush-app:qwerty-secret@localhost:8080/oauth/token` and put this user data to the body: `grant_type: password`, `user: alice`, `password: alice1`

#### How to renew jwt token
To renew access token change request body to `grant_type: refresh_token` and put `refresh_token: <refresh-token>`
____

To create asymmetric keys 
`keytool -genkeypair -alias mykeys -keyalg RSA -keypass mypass -keystore mykeys.jks -storepass mypass`
