package gmail.aliveairush.authorizationserver.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.provider.token.DefaultUserAuthenticationConverter;

import java.util.Map;

// This class needed to enhance oauth with user_id
public class CustomUserAuthenticationConverter extends DefaultUserAuthenticationConverter {

    private static String USER_ID = "user_id";

    @Override
    public Map<String, ?> convertUserAuthentication(Authentication authentication) {
        Map<String, Object> response = (Map<String, Object>) super.convertUserAuthentication(authentication);
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        response.put(USER_ID, userDetails.getUserId());
        return response;
    }
}
