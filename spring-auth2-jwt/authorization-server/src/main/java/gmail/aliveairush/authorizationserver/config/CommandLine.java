package gmail.aliveairush.authorizationserver.config;

import gmail.aliveairush.authorizationserver.model.Role;
import gmail.aliveairush.authorizationserver.model.User;
import gmail.aliveairush.authorizationserver.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;

@Configuration
public class CommandLine implements CommandLineRunner {

    @Autowired
    private UserRepository userRepository;

    @Override
    @Transactional
    public void run(String... args) throws Exception {
        User admin = new User(
                "alice",
                "alice@gmail.com",
                "alice1",
                Collections.singletonList(Role.ROLE_ADMIN));

        User user = new User(
                "bob",
                "bob@gmail.com",
                "bob1",
                Collections.singletonList(Role.ROLE_CLIENT));

        userRepository.save(admin);
        userRepository.save(user);
    }
}
