package gmail.aliveairush.authorizationserver.security;

import gmail.aliveairush.authorizationserver.security.CustomUserAuthenticationConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.*;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory;

@Configuration
@EnableAuthorizationServer
public class OAuth2AuthorizationConfig extends AuthorizationServerConfigurerAdapter {

    @Value("${security.oauth2.jwt.access-token-validity-seconds}")
    private int accessTokenValiditySeconds;

    @Value("${security.oauth2.jwt.refresh-token-validity-seconds}")
    private int refreshTokenValiditySeconds;

    @Value("${security.oauth2.client.client-id}")
    private String oauthClientId;

    @Value("${security.oauth2.client.client-secret}")
    private String oauthClientSecret;

    @Autowired
    private AuthenticationManager authManager;

    @Value("${security.oauth2.resource.id}")
    private String resourceId;

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints){
        endpoints
                .accessTokenConverter(accessTokenConverter())
                .authenticationManager(authManager)
                .tokenStore(tokenStore());
    }

    // Defining who can access to get token by "/oauth/token" endpoint
    @Override
    public void configure(AuthorizationServerSecurityConfigurer oauthServer) throws Exception {
        oauthServer
                .tokenKeyAccess("hasAuthority('ROLE_TRUSTED_CLIENT')")
                .checkTokenAccess("hasAuthority('ROLE_TRUSTED_CLIENT')");
    }

    // Configure ClientDetailsService, declaring clients and their props
    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients.inMemory()
                .withClient(oauthClientId)
                .secret(oauthClientSecret)
                .authorizedGrantTypes("client_credentials", "password", "refresh_token")
                .authorities("ROLE_TRUSTED_CLIENT")
                .scopes("read", "write")
                .resourceIds(resourceId)
                .accessTokenValiditySeconds(accessTokenValiditySeconds)
                .refreshTokenValiditySeconds(refreshTokenValiditySeconds);
    }

    @Bean
    public TokenStore tokenStore() {
        // Actually not a store. It translates Jwt to and from authentication.
        return new JwtTokenStore(accessTokenConverter());
    }

    // Translates in both direction JWT token values and OAuth values information, plus enhance Authentication
    @Bean
    public JwtAccessTokenConverter accessTokenConverter() {
        // Translates Auth to and from JWT
        JwtAccessTokenConverter jwtAccessTokenConverter = new JwtAccessTokenConverter();
        // converter.setSigningKey("key"); Symmetric key
        KeyStoreKeyFactory keyStoreKeyFactory =
                new KeyStoreKeyFactory(
                        new ClassPathResource("jwtkeys.jks"),
                        "jwtqwertypass".toCharArray());
        jwtAccessTokenConverter.setKeyPair(keyStoreKeyFactory.getKeyPair("jwtkeys"));

        // Join together other converter, which enhance authentication with User_id
        DefaultAccessTokenConverter accessTokenConverter = new DefaultAccessTokenConverter();
        accessTokenConverter.setUserTokenConverter(new CustomUserAuthenticationConverter());
        jwtAccessTokenConverter.setAccessTokenConverter(accessTokenConverter);

        return jwtAccessTokenConverter;
    }
}
