### Spring & OAuth2 & JWT
___

This is a sample of Spring OAuth2 and JWT, project divided into two servers for better understanding how Centralized Authorization works.

**Authorization Server** - returns enhanced(by User_id) JWT token. Trusted clients configured in memory, but users stored in Postgres.

**Resourse Server** - accepts JWT and convert it to default UsernamePasswordAuthentication, so we can access secured resources.
