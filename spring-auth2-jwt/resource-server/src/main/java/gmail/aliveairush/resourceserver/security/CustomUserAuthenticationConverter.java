package gmail.aliveairush.resourceserver.security;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.oauth2.provider.token.DefaultUserAuthenticationConverter;
import org.springframework.util.StringUtils;

import java.util.Collection;
import java.util.List;
import java.util.Map;

// This class needed to enhance authentication with user_id
public class CustomUserAuthenticationConverter extends DefaultUserAuthenticationConverter {
    private final static String USER_ID = "user_id";
    private final static String USER_NAME = "user_name";
    private final static String AUTHORITIES = "authorities";

    @Override
    public Authentication extractAuthentication(Map<String, ?> map) {
        if (map.containsKey(USER_ID)) {
            Long userId = Long.valueOf((Integer) map.get(USER_ID));
            String username = (String) map.get(USER_NAME);
            List<? extends GrantedAuthority> authorities = getAuthorities(map);

            UserDetailsImpl userDetails = new UserDetailsImpl(userId, username, null, authorities);

            return new UsernamePasswordAuthenticationToken(userDetails, "", authorities);
        }
        return null;
    }

    private List<? extends GrantedAuthority> getAuthorities(Map<String, ?> map) {
        Object authorities = map.get(AUTHORITIES);

        if (authorities instanceof Collection)
            return AuthorityUtils.commaSeparatedStringToAuthorityList(
                    StringUtils.collectionToCommaDelimitedString((Collection<?>) authorities));

        throw new IllegalArgumentException("Authorities must be a Collection");
    }
}
