package gmail.aliveairush.resourceserver.controllers;

import gmail.aliveairush.resourceserver.model.User;
import gmail.aliveairush.resourceserver.repository.UserRepository;
import gmail.aliveairush.resourceserver.security.UserDetailsImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.Optional;

@RestController
public class GeneralController {

    private Logger logger = LoggerFactory.getLogger(GeneralController.class);

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/")
    public RestMsg hello() {
        return new RestMsg("Hello World!");
    }

    @GetMapping("/api/test")
    public RestMsg apitest() {
        return new RestMsg("Hello apiTest!");
    }

    @GetMapping(value = "/api/hello")
    public ResponseEntity<?> helloUser(Principal principal) {
        logger.debug("Request form {}", principal.getName());
        Optional<User> user = userRepository.findByUsername(principal.getName());
        return ResponseEntity.ok(user);
    }

    // example of request with Authentication with additional fields;
    @GetMapping("/api/admin")
    public RestMsg helloAdmin() {
        UserDetailsImpl userDetails = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        logger.debug("Request from {}", userDetails.getUsername());
        return new RestMsg(String.format("Welcome  %s,  user_id='%d,'!", userDetails.getUsername(), userDetails.getUserId()));
    }

    // A helper class to make our controller output look nice
    public static class RestMsg {
        private String msg;

        public RestMsg(String msg) {
            this.msg = msg;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }
    }
}
