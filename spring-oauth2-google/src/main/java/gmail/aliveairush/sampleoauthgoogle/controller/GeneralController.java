package gmail.aliveairush.sampleoauthgoogle.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
public class GeneralController {

    private Logger logger = LoggerFactory.getLogger(GeneralController.class);

    @GetMapping("/user")
    public ResponseEntity<?> getHello(Principal principal){
        logger.debug("Got principal for {}", principal.getName());
        return ResponseEntity.ok(principal.getName());
    }
}
