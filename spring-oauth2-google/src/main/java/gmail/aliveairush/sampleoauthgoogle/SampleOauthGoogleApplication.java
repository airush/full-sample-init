package gmail.aliveairush.sampleoauthgoogle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampleOauthGoogleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SampleOauthGoogleApplication.class, args);
    }

}
