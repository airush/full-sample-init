package gmail.aliveairush.sampleoauthgoogle.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "usr")
@Data
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(unique = true)
    private String username;
    @Column(unique = true)
    private String email;
    private String password;
    private String picture;
    private String firstName;
    private String lastName;
    private String gender;
    private String locale;
}
