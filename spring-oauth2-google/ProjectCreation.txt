
### Register project on google dev console
Firstly we need to register our project and create Client-id and Client Secret
https://console.cloud.google.com/apis/credentials

"Create Project" -> Fill "Project name" field and "Location". -> Create button.

####Create keys
"Navigation bar"-"Api & Services"-"Credentials" -> "+ Create Credentials" ->  choose "Web application" ,fill name -> "Create button" -> Copy created Client id and Secret to your project

Click to created app -> "Authorized JavaScript origins" set there your machine host(in this example http://localhost:8082) -> "Authorized redirect URIs" path where to redirect  when user try to access protected resources(this case: http://localhost:8082/login - default google form)


